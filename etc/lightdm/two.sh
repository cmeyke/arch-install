#!/bin/sh
nvidia-settings --assign CurrentMetaMode="DVI-D-0:2560x1600_60 +0+0 { ForceFullCompositionPipeline = On }, DP-4:3440x1440_100 +2560+0 { ForceFullCompositionPipeline = On, AllowGSYNCCompatible=On }"

xrandr --output DVI-D-0 --rate 60 --mode 2560x1600 --pos 0x0 --rotate normal --output DP-4 --rate 100 --primary --mode 3440x1440 --pos 2560x0 --rotate normal
